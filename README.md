# README #

### Whats in package? ###

* Nginx
* Apache
* PostgreSQL
* mod_wsgi

### Usage ###

Run:
```
sudo sh startup.sh up
```

Stop:
```
sudo sh startup.sh down
```

### Whats next? ###

Passwords for PSQL you can find at 'containers/apache/sql/10-init.sql' after first UP containers

### Known issues ###
Now it's works only with default 'djanapp' project