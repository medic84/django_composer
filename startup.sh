#!/bin/bash

if [ "$EUID" -ne 0 ]; then
    echo "Please run script as root"
    exit
fi

if [[ "$1" == "revert" ]]; then
    rm .docker-lock
    cp -f .origin/settings.py projects/djanapp/djanapp/settings.py
    rm containers/apache/requirements.txt > /dev/null 2>&1
    rm containers/db/sql/*.sql > /dev/null 2>&1
    rm logs/*/*.log > /dev/null 2>&1
    echo "Success reverting to init"
    exit
fi

if [[ "$1" == "down" ]]; then
    docker-compose down
    exit
fi

if [[ "$1" == "reload" ]]; then
    docker-compose exec web service apache2 reload
    docker-compose exec nginx service nginx restart
    docker-compose exec db service postgresql reload
    echo "Success reload servers"
    exit
fi

if [[ "$1" == "up" ]]; then

    if [ ! -f .docker-lock ]; then
        rootpasswd="$(openssl rand -hex 6)"
        userpasswd="$(openssl rand -hex 4)"

        echo "Set Django Database & User Settings..."
        sed -i "s/'ENGINE': 'django.db.backends.sqlite3',/'ENGINE': 'django.db.backends.postgresql',\n        'HOST': 'db',\n        'PORT': 5432,/" projects/djanapp/djanapp/settings.py
        sed -i "s/'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),/'NAME': 'djanapp',/" projects/djanapp/djanapp/settings.py
        sed -i "s/'PASSWORD': '',/'PASSWORD':'${userpasswd}',/" projects/djanapp/djanapp/settings.py
        sed -i "s/'USER': '',/'USER': 'django_user',/" projects/djanapp/djanapp/settings.py
        sed -i "s/ALLOWED_HOSTS = \[''\]/ALLOWED_HOSTS = \['\*'\]/" projects/djanapp/djanapp/settings.py

        echo "Initializing init SQL"
        mkdir -p containers/db/sql > /dev/null 2>&1
        echo "ALTER USER postgres WITH PASSWORD '${rootpasswd}';" > containers/db/sql/10-init.sql
        echo "CREATE DATABASE djanapp;" >>containers/db/sql/10-init.sql
        echo "CREATE USER django_user WITH PASSWORD '${userpasswd}';" >> containers/db/sql/10-init.sql
        echo "GRANT ALL PRIVILEGES ON DATABASE 'djanapp' to django_user;" >> containers/db/sql/10-init.sql

        for entry in projects/*/requirements.txt
        do
            cat ${entry} >> containers/apache/requirements.txt
            echo "" >> containers/apache/requirements.txt
        done
    fi

    docker-compose up -d

    if [ ! -f .docker-lock ]; then
        docker-compose exec web python /home/djanapp/manage.py migrate
        rm containers/apache/requirements.txt > /dev/null 2>&1
        touch .docker-lock
    fi
    exit
fi

echo "No arguments given... Exit."